package com.that_quick.tq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class TQServices extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tqservices);

        LinearLayout accounttab = findViewById(R.id.llaccount);
        accounttab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getApplicationContext(), MainActivity.class));

            }
        });


        LinearLayout llcleaning = findViewById(R.id.llcleaning);
        llcleaning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                VarData.tqservice = "Cleanning";
                startActivity(new Intent(getApplicationContext(), TQSubServices.class));

            }
        });


        LinearLayout llplumbing = findViewById(R.id.llplumbing);
        llplumbing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                VarData.tqservice = "Plumbing";
                startActivity(new Intent(getApplicationContext(), TQSubServices.class));

            }
        });


    }
}
