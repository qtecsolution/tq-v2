package com.that_quick.tq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignUP extends Fragment {



    private Button signUpButton, signIn;
    private EditText fnameEdittext, lnameEdittext,
            emailEdittext, phoneEdittext, passEditText;


    GetDataService service;


    public SignUP() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.signup, container, false);


        fnameEdittext = v.findViewById(R.id.first_name_etxt);
        lnameEdittext = v.findViewById(R.id.last_name_etxt);
        emailEdittext = v.findViewById(R.id.email_etxt);
        phoneEdittext = v.findViewById(R.id.mobilenum_etxt);
        passEditText = v.findViewById(R.id.password_etxt);


        final Button sign_up_btn = v.findViewById(R.id.signupbutton);

        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        sign_up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.getId() == R.id.signupbutton) {

                    String fname = fnameEdittext.getText().toString();
                    String lname  = lnameEdittext.getText().toString();
                    String email = emailEdittext.getText().toString();
                    String phone = phoneEdittext.getText().toString();
                    String pass = passEditText.getText().toString();


                    if (fname.isEmpty()) {
                        fnameEdittext.setError(" FirstName Missing");
                        fnameEdittext.requestFocus();
                    }
                    if (lname .isEmpty()) {
                        lnameEdittext.setError("LastName Missing");
                        lnameEdittext.requestFocus();
                    }
                    if (email.isEmpty()) {
                        emailEdittext.setError("Email Missing");
                        emailEdittext.requestFocus();
                    }
                    if (phone.isEmpty()) {
                        phoneEdittext.setError("PhoneNumber Missing");
                        phoneEdittext.requestFocus();
                    }
                    if (pass.isEmpty()) {
                        passEditText.setError("Password Missing");
                        passEditText.requestFocus();
                    }


/*

        if (name.isEmpty()||email.isEmpty()||userName.isEmpty()||password.isEmpty()){
            Toast.makeText(this, "Data missing", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "SignUp Successfully", Toast.LENGTH_SHORT).show();
        }

*/ // sending data
                    signupData(fname,
                            lname ,
                            email,
                            phone,
                            pass);


                } else if (v.getId() == R.id.logInButtonId) {
                    LogIn logIn = new LogIn();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.mainLayout,
                            logIn, logIn.getTag()).commit();

                }


            }


        });


        return v;
    }


    public void signupData(String FirstName, String LastName, String Email, String PhoneNumber, String Password) {

        Log.d("data", FirstName + ", -" + LastName + ", -" + Email + ", -" + PhoneNumber + ", -" + Password);


        service.createUser(FirstName, LastName, Email, PhoneNumber, Password).enqueue(new Callback<RegistrationModel>() {

            @Override
            public void onResponse(Call<RegistrationModel> call, Response<RegistrationModel> response) {


                Log.d("ebb", response.errorBody()+"");


                try {
                    if (response.body().getIsSuccess() == 1) {

                        Intent intent = new Intent(getActivity(), Profile.class);
                        startActivity(intent);

                        Toast.makeText(getContext(), "Successfully account created!",
                                Toast.LENGTH_LONG).show();
                    }

                } catch (NullPointerException e){

                    Toast.makeText(getContext(), "Creating account failed!" ,
                            Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<RegistrationModel> call, Throwable t) {

                Toast.makeText(getContext(), "Creating account failed!", Toast.LENGTH_SHORT).show();

                Log.d("res-p", t.getMessage()+" == "+t.toString());

            }
        });


    }
}

